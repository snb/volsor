# Generated by Django 2.1 on 2018-10-10 18:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('volsor', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='currency',
            options={'ordering': ('name',)},
        ),
        migrations.AlterModelOptions(
            name='rate',
            options={'ordering': ('-timestamp', 'currency')},
        ),
        migrations.RenameField(
            model_name='rate',
            old_name='currency_from',
            new_name='currency',
        ),
        migrations.RemoveField(
            model_name='rate',
            name='currency_to',
        ),
    ]
