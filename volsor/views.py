from datetime import datetime, timedelta

from rest_framework import viewsets
from rest_framework.response import Response

from volsor.models import Currency, Rate
from volsor.serializers import CurrencySerializer


class CurrencyViewSet(viewsets.ModelViewSet):
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer
    http_method_names = ['get']


class RateViewSet(viewsets.ViewSet):
    http_method_names = ['get']

    def list(self, request, *args, **kwargs):
        def get_rate(currency, date):
            queryset = Rate.objects\
                .filter(currency__name=currency)\
                .filter(timestamp__lt=date)
            rates = queryset.all()
            return rates[0].value if rates else None

        params = request.query_params
        currency_from = params.get('from')
        currency_to = params.get('to')
        date_str = params.get('date')
        if not currency_from or not currency_to or not date_str:
            return Response('Both currencies and data should be sent.',
                            status=400)

        if currency_from == currency_to:
            result = 1
        else:
            date = datetime.strptime(date_str, '%Y-%m-%d')
            date_next = date + timedelta(days=1)
            rate_from = 1 if currency_from == 'USD' else \
                get_rate(currency_from, date_next)
            rate_to = 1 if currency_to == 'USD' else \
                get_rate(currency_to, date_next)

            if not rate_from or not rate_to:
                return Response('Rate is unavailable.', status=400)
            result = rate_to / rate_from

        return Response(result)
