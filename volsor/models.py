from django.db import models


class Currency(models.Model):
    name = models.CharField(max_length=20)
    description = models.CharField(max_length=200)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Rate(models.Model):
    currency = models.ForeignKey(
        Currency, on_delete=models.PROTECT, related_name='rates_from')
    timestamp = models.DateTimeField()
    value = models.DecimalField(max_digits=12, decimal_places=6)

    class Meta:
        ordering = ('-timestamp', 'currency')

    def __str__(self):
        return '%s on %s' % (self.currency.name, self.timestamp)
