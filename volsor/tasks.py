from __future__ import absolute_import, unicode_literals

from datetime import datetime
import json
import requests

from django.db import transaction
from django.conf import settings

from volsor.celery import app
from volsor.models import Currency, Rate


@app.task
def load_rates():
    r = requests.get(
        'https://openexchangerates.org/api/latest.json?app_id=%s' %
        settings.OPENEXCHANGE_ID)
    if r.status_code != 200:
        raise requests.HTTPError('Error %s: %s' % (r.status_code, r.reason))
    data = json.loads(r.text)
    if data['base'] != 'USD':
        raise ValueError('Base currency is not USD')
    rates = data['rates']
    dt = datetime.fromtimestamp(data['timestamp'])
    currencies = [(obj.id, obj.name) for obj in Currency.objects.all()]
    count = 0

    with transaction.atomic():
        for id, name in currencies:
            if name in rates and not len(Rate.objects.filter(currency_id=id,
                                                             timestamp=dt)):
                rate = Rate(
                    currency_id=id, timestamp=dt, value=rates[name])
                rate.save()
                count += 1
    return count
