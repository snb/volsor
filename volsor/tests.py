from datetime import datetime
import json
import unittest

from django.conf import settings
from rest_framework.test import RequestsClient

from volsor.models import Currency, Rate
from volsor.tasks import load_rates


class TestApi(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestApi, self).__init__(*args, **kwargs)
        settings.DEBUG = True

    def setUp(self):
        self.yyt = '2018-01-01'
        self.yt = '2018-01-02'
        self.dt = '2018-01-03'
        self.ft = '2018-01-04'
        Currency.objects.get_or_create(name='USD', description='US Dollar')
        eur, is_new = Currency.objects.get_or_create(name='EUR',
                                                     description='Euro')
        pln, is_new = Currency.objects.get_or_create(
            name='PLN', description='Polish Zloty')
        Rate.objects.get_or_create(currency=pln,
                                   timestamp=datetime.strptime(self.dt,
                                                               '%Y-%m-%d'),
                                   value=2)
        Rate.objects.get_or_create(currency=pln,
                                   timestamp=datetime.strptime(self.yt,
                                                               '%Y-%m-%d'),
                                   value=4)
        Rate.objects.get_or_create(currency=eur,
                                   timestamp=datetime.strptime(self.dt,
                                                               '%Y-%m-%d'),
                                   value=8)
        Rate.objects.get_or_create(currency=eur,
                                   timestamp=datetime.strptime(self.yt,
                                                               '%Y-%m-%d'),
                                   value=16)
        self.client = RequestsClient()

    def test_currencies(self):
        response = self.client.get('http://testserver/api/currencies')
        data = json.loads(response.text)
        self.assertListEqual(data, [
            {'name': 'EUR', 'id': 2, 'description': 'Euro'},
            {'name': 'PLN', 'id': 3, 'description': 'Polish Zloty'},
            {'name': 'USD', 'id': 1, 'description': 'US Dollar'},
        ])

    def test_rates(self):
        # current rate
        response = self.client.get(
            'http://testserver/api/rates/?from=USD&to=EUR&date=%s' % self.dt)
        self.assertEqual(float(response.text), 8)

        # request future date, should be the same
        response = self.client.get(
            'http://testserver/api/rates/?from=USD&to=EUR&date=%s' % self.ft)
        self.assertEqual(float(response.text), 8)

        # and back
        response = self.client.get(
            'http://testserver/api/rates/?from=EUR&to=USD&date=%s' % self.dt)
        self.assertEqual(float(response.text), 1 / 8)

        # yesterday's rate
        response = self.client.get(
            'http://testserver/api/rates/?from=USD&to=EUR&date=%s' % self.yt)
        self.assertEqual(float(response.text), 16)

        # and back
        response = self.client.get(
            'http://testserver/api/rates/?from=EUR&to=USD&date=%s' % self.yt)
        self.assertEqual(float(response.text), 1 / 16)

        # not USD
        response = self.client.get(
            'http://testserver/api/rates/?from=PLN&to=EUR&date=%s' % self.dt)
        self.assertEqual(float(response.text), 4)

        # and back
        response = self.client.get(
            'http://testserver/api/rates/?from=EUR&to=PLN&date=%s' % self.dt)
        self.assertEqual(float(response.text), 1 / 4)

        # same currency
        response = self.client.get(
            'http://testserver/api/rates/?from=EUR&to=EUR&date=%s' % self.dt)
        self.assertEqual(float(response.text), 1)

        # not enough data
        response = self.client.get('http://testserver/api/rates')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.text,
                         '"Both currencies and data should be sent."')

        # missed currency
        response = self.client.get(
            'http://testserver/api/rates/?from=BAD&to=EUR&date=%s' % self.dt)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.text, '"Rate is unavailable."')

        # request past
        # we use the same message, it can be enhanced further to separate
        # missed currencies and wrond dates
        response = self.client.get(
            'http://testserver/api/rates/?from=PLN&to=EUR&date=%s' % self.yyt)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.text, '"Rate is unavailable."')

    def test_loading(self):
        result = load_rates()
        self.assertEqual(result, 3)  # loaded 3 rates
        rates = Rate.objects.filter(
            timestamp__gt=datetime.strptime(self.ft, '%Y-%m-%d'))
        names = set(rate.currency.name for rate in rates)
        self.assertSetEqual(names, set(('USD', 'EUR', 'PLN')))
