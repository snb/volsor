var v = new Vue({
  el: '#app',
  delimiters: ['${','}'],
  data: {
    options: [
      {name: 'value1'},
      {name: 'value2'},
      {name: 'value3'}
    ],
    currencies: [],
    loading: true,
    from: '',
    to: '',
    date: '',
    value: '',
  },
  created() {
      let api_url = 'api/currencies/?format=json';
      this.loading = true;
      this.$http.get(api_url)
          .then((response) => {
            this.currencies = response.data.map(o => o.name);
            this.loading = false;
          })
          .catch((err) => {
            this.loading = false;
            console.log(err);
          })
  },
  methods: {
    convert() {
      if (!this.from || !this.to || !this.date) {
        this.value = 'Unknown';
        return;
      }
      let api_url = 'api/rates/?format=json';
      this.loading = true;
      api_url += '&from=' + this.from;
      api_url += '&to=' + this.to;
      api_url += '&date=' + this.date;
      this.$http.get(api_url)
          .then((response) => {
            this.loading = false;
            this.value = '1 ' + this.from + ' = ' + response.data.toFixed(6) + ' ' + this.to;
          })
          .catch((err) => {
            this.loading = false;
            this.value = err.body;
            console.log(err);
          })
    }
  }
});
