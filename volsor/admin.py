from django.contrib import admin
from volsor.models import Currency, Rate

admin.site.register(Currency, admin.ModelAdmin)
admin.site.register(Rate, admin.ModelAdmin)
