# Volsor API

Test application to get currency rates and convert currencies.

## Install

    docker-compose build

## Run

    docker-compose up runserver

## Test

    docker-compose up autotests

## Usage

`http://0.0.0.0` - main page

`http://0.0.0.0/admin/` admin

`http://0.0.0.0/api/` available API

`http://0.0.0.0/api/currencies/` get currency list

`http://0.0.0.0/api/rates/?from=CZK&to=EUR&date=2018-10-11` - get conversion rate
